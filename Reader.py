import glob
import pandas as pd
import dask.dataframe as dd

    
def find_files(filetype, input_path, recursive = True):
    """Finds all files with defined ending (type_ending) in the given folder (input_path).
    
    Paramenters
    -----------
    filetype : str
        last part of the filename, defining its type. i.e. '.csv'
    input_path : str
        path where the files should be searched (with / at the end)
    recursive : boolean
        sort list in recursive order or not
        (optional, default True)
    
    Returns
    -------
    list
        sorted list containing all file path
    """
    files = glob.glob(input_path + '**/*' + filetype, recursive = True)
    files.sort(reverse=True)
    return files

def find_highest_version_files(filetype, input_path):
    """Finds all newest files with defined ending (type_ending) in the given folder (input_path).
    The filename has to be in the Format:
        examplename-v3-p2.csv
    where all combinations of -p and -v except of '-v' followed by '-v' are possible. 
    The Numbers after -v and -p are the version and partition 
    
    Paramenters
    -----------
    filetype : str
        last part of the filename, defining its type. i.e. '.csv'
    input_path : str
        path where the files should be searched (with / at the end)
    
    Returns
    -------
    list
        reverse sorted list with all file path of the newest versions in it
    """
    #find all files and sort them in reverse order
    #(this is important to find the newest version here)
    all_files = find_files(filetype, input_path)
    old_versions = []
    #set the first file als current highest version
    highest_version = all_files[0]
    #iterate throught all found files and compare if it is a newer version
    for file in all_files[1:]:
        #find the first filename that is not equal in both path.
        #the type_ending is replaced, so it could not interfer the name comparison
        highest_path_seperations = highest_version.split('/')
        highest_path_seperations[-1] = highest_path_seperations[-1].replace(filetype, '')
        current_file_path_seperations = file.split('/')
        current_file_path_seperations[-1] = current_file_path_seperations[-1].replace(filetype, '')
        i = 0
        while (i < min(len(highest_path_seperations), len(current_file_path_seperations))) & (
            highest_path_seperations[i] == current_file_path_seperations[i]):
            i = i + 1

        #in that filename find the first not equal version
        splitted_by_version1 = highest_path_seperations[i].split('-v')
        splitted_by_version2 = current_file_path_seperations[i].split('-v')
        j = 0
        while splitted_by_version1[j] == splitted_by_version2[j]:
            if j + 1 < min(len(splitted_by_version1),len(splitted_by_version2)):
                j = j + 1
            else:
                #if all names are equal till the end of one path,
                #the first is a not partitioned version of the second
                old_versions.append(highest_version)
                highest_version = file
                j = -1
                break
        #if j is not -1 the versions are different at some index j
        if j != -1:
            #find the first partition in a version that is different
            splitted_by_partition1 = splitted_by_version1[j].split('-p')
            splitted_by_partition2 = splitted_by_version2[j].split('-p')
            k = 0
            while splitted_by_partition1[k] == splitted_by_partition2[k]:
                if k + 1 < min(len(splitted_by_partition1),len(splitted_by_partition2)):
                    k = k + 1
                else:
                    #if all names are equal till the end of one path,
                    #there are 2 options with a new highest version:
                    #1. j = 0 means the names in front of the first '-v' are different.
                    #   This is the case if current name includes more '-p' in front of the first '-v' 
                    #   than highest name. If highest name contains at least one '-v', 
                    #   the current is the first version of the highest and thus older.
                    #2. The current version string contains more partitions than the highest
                    #   but highest is a newer version of the last partition (more '-v' than the first different at j)
                    if ((j == 0) & (len(splitted_by_version1) != 1)) or (
                        (len(splitted_by_partition2) > k + 1) & (len(splitted_by_partition1) == k + 1) & (
                            len(splitted_by_version1) > j + 1)):
                        old_versions.append(file)
                    # in all other cases for equal names till the end of one filepath,
                    # current is newer than the highest.
                    else:
                        old_versions.append(highest_version)
                        highest_version = file
                    k = -1
                    break
                    
            if k == 0:
                if j == 0:
                    #k = 0 and j = 0 means the filename without '-v' and '-p' terms is already unequal
                    highest_version = file
                else:
                    #if k = 0 but j is not, the version number is different and thus the current file older
                    old_versions.append(file)
            #if k is not 0 or -1 current and highest are different partitions from the same version of a file
            elif k != -1:
               highest_version = file

    #remove all old versions from the list and return the remaining newest versions
    for path in old_versions:
        all_files.remove(path)
    return all_files
    
def pd_read(file, filetype, **type_arguments):
    """Reading file of a given typ (filetype) to a pandas (pd) Dataframe
    or a TextFileReader with DataFrames in it.
    
    Parameters
    ----------
    file : str
        filepath to the file to read
    filetype : str
        the filetype you want to read in.
        (possible types: '.csv', '.hdf', '.parquet')
    **type_arguments
        arguments for the type specific read function from pandas
        
    Returns
    -------
    pandas DataFrame or TextFileReader
        DataFrame constructed from the file 
        or TextFileReader with multiple DataFrames (chunks) 
    """
    match filetype:
        case '.csv':
            return pd.read_csv(file, **type_arguments)
        case '.parquet':
            return pd.read_parquet(file, **type_arguments)
        case '.hdf':
            return pd.read_hdf(file, **type_arguments)
            
def dd_read(file, filetype, **type_arguments):
    """Reading file of a given typ (filetype) to a dask Dataframe (dd).
    
    Parameters
    ----------
    file : str
        filepath to the file to read
    filetype : str
        the filetype you want to read in.
        (possible types: '.csv', '.hdf', '.parquet')
    **type_arguments
        arguments for the type specific read function from dask
        
    Returns
    -------
    lazy dask DataFrame
        a lazy Object that could be computed to a DataFrame with .compute()
    """
    match filetype:
        case '.csv':
            return dd.read_csv(file, **type_arguments)
        case '.parquet':
            return dd.read_parquet(file, **type_arguments)
        case '.hdf':
            return dd.read_hdf(file, **type_arguments)
