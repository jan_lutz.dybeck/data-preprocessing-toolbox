def generate_version_name(file_name, filetype, partition_number = None, new_version = True, digits = 2):
    """generate the names for new versions and/or partitions with a given size of n digits.
    Where n is by default 2 (versions 01-99)
    
    Parameters
    ----------
    file_name : str
        path to the file including the files name
    filetype : str 
        string at the end of the file. i.e. '.csv'
    partition_number : int or None
        Number of partitions that should be generated from the original file
        (optional, default = None)
    new_version : boolean
        True if a new version should be generated or False if not.
        (optional, default = True)
    digits : int
        amount of digits the version strings should have.
        digits = 2 -> -v01, digits = 3 -> -v001 
        (optional, default = 2)

    Returns
    -------
    list or str
        new generated version name or list of new names for partitions
    """
    #split file_name in the name and the path to change version
    new_file_name = file_name.split('/')[-1].replace(filetype,'')
    input_path = file_name.removesuffix(file_name.split('/')[-1])
    #if a new version should be generated
    if new_version == True:
        #split at the last '-v'
        old_version = new_file_name.rsplit('-v', 1)
        #If the old name has no '-p' after the last '-v' and it contains at least one '-v',
        #the version string could be updated
        if (len(old_version[-1].split('p')) == 1) & (len(new_file_name.split('-v')) != 1):
            #increase Version by one and fill up with zeros till digits is reached
            version = str(int(old_version[-1]) + 1)
            missing_zeros = digits - len(version)
            #if the version has more digits than it should have
            if missing_zeros < 0:
                return 'not enought digits'
            else:
                z = 0
                zeros = ''
                while z < missing_zeros:
                    zeros = zeros + '0'
                    z = z + 1
                version = zeros + version
            new_file_name = input_path + old_version[0] + '-v' + version
        #if the '-v' is not at the end of the file_name, add a new one starting with version 1
        else:
            z = 0
            zeros = ''
            while z < digits - 1:
                zeros = zeros + '0'
                z = z + 1
            new_file_name = input_path + new_file_name + '-v' + zeros + '1'

    #generate partition names if wanted
    if partition_number != None:
        new_file_name_list = []
        i = 1
        while i <= partition_number:
            new_file_name_list.append(input_path + new_file_name + '-p' + str(i) + filetype)
            i = i + 1
            
    #return new name or list of new names        
    if partition_number != None:
        return new_file_name_list
    else:
        return new_file_name + filetype
    
    
def write(df, filetype, file_name, **type_arguments):
    """write a pandas or dask DataFrame to file with name: file_name
    
    Parameters
    ----------
    df : pandas or dask DataFrame
        the DataFrame that should be saved
    filetype : str
        the filetype you want to read in.
        (possible types: '.csv', '.hdf', '.parquet')
    file_name : str
        name of the file
    **type_arguments
        arguments for the type specific write function from pandas or dask
    
    """
    match filetype:
        case '.csv':
            df.to_csv(file_name, **type_arguments)
        case '.parquet':
            df.to_parquet(file_name, **type_arguments)
        case '.hdf':
            df.to_hdf(file_name, **type_arguments)
