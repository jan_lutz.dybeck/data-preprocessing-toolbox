# Data Preprocessing Toolbox

<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""></a></p>  

**note: The license above does not include licenses for other programms or projects used in this work. It only licenses work that was done by the author.**

## Table of contents
1. [Introduction](#introduction)
2. [Strucuture of Folder Tree](#folderstructure)
3. [Known Problems](#bugs)
4. [Publications & Links](#publications)
5. [Contributers](#contributors)
6. [Funding](#funding)

## 1. Introduction <a name="introduction"></a>

This repository is a collection of functions for data preprocessing. 
The Toolbox is based on pandas [2] and dask [3]. All Python files are independend of the other. One modul (Python file with functions) could be used without other moduls.
This work was started as a part of a Master Proposal at TU Darmstadt, Laser- and Plasmaphysics [5].

## 2. Structure of Folder Tree <a name="folderstructure"></a>

Contained notebooks for examples:
* `Example_Reader.ipynb`: Jupyter Notebook with example for READER modul.
* `Example_Multiprocess.ipynb`: Jupyter Notebook with example for Multiprocess modul.
* `Example_Error_Finder.ipynb`: Jupyter Notebook with example for Error_Finder modul.
* `Example_Editor.ipynb`: Jupyter Notebook with example for Editor modul.
* `Example_Writer.ipynb`: Jupyter Notebook with example for Writer modul.

Python files with functions (modules):
* `Reader.py`: Python file with functions for reading data.
* `Multiprocess.py`: Python file with functions for parallel processing.
* `Error_Finder.py`: Python file with functions for type-error detection.
* `Editor.py`: Python file for editing the data.
* `Writer.py`: Python file for writing data to a file. 

Example configuration File:
* `example.ini` 

## 4. Known Problems <a name="bugs"></a>
When using dask [3] or pandas [2] with specified column types, make sure there are no type errors in the data file.  
The data must fit in your memory. It is possible to controll memory usage for big datasets with dask or with Multiprocess in combination with chunked pandas DataFrames.


## 5. Publications & Links <a name="publications"></a>

[1] [GitLab](https://git.rwth-aachen.de/jan_lutz.dybeck/data-preprocessing-toolbox)

[2] [pandas](https://doi.org/10.5281/zenodo.10537285)

[3] [dask](http://dask.pydata.org)

[4] [numpy](https://numpy.org/doc/stable/)

[5] [Laser- and Plasmaphysics, TU Darmstadt](https://www.ikp.tu-darmstadt.de/forschung_kernphysik/gruppen_kernphysik/experiment/ag_m_roth/index.de.jsp)

## 6. Contributors <a name="contributors"></a>
Contributors include (alphabetically): 
*   J. L. Dybeck

## 7. Funding <a name="funding"></a>
* Started as a part of a Master Proposal at TU Darmstadt [5]
