import pandas as pd
import configparser
import numpy as np
import uuid

def map_columns(pd_df, config_path):
    """Rename the columns based on the configuration file section variables.
    If a column does not exist a new could be created.
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        Frame to change
    config_path : str
        path to the configuration file (including its name)
    
    """
    config = configparser.ConfigParser()
    config.read(config_path)
    new_names = {}
    new_columns = {}
    #for all variables in the configuration file:
    #if the variable is linked to a column, add it to new_names,
    #if not add a new empty (np.nan) column to new columns
    for variable in config.options('variables'):
        if eval(config.get('variables', variable))[1] != None:
            new_names[eval(config.get('variables', variable))[1]] = eval(config.get('variables', variable))[0]
        else: 
            new_columns[eval(config.get('variables', variable))[0]] = np.nan  
    #rename all existing columns and add new columns after that.
    pd_df.rename(columns = new_names, inplace = True)
    set_columns(pd_df, new_columns)
    
def add_constants(pd_df, config_path, selection = None):
    """Add constants from the configuration file at config_path
    to the pandas DataFrame (pd_df)
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        Frame to change
    config_path : str
        path to the configuration file (including its name)
    selection : list or None
        Selected columns names that should be added.
        If None: all columns in the constant section of
        configuration file will be added.
        (optional, default: None)
    """
    config = configparser.ConfigParser()
    config.read(config_path)
    new_columns = {}
    #if no selection is given, get all constants from the 
    #configuration file
    if selection == None:
        selection = config.options('constants')
    #Add a column with the constant if
    #the column does not exist already.
    for constant in selection:
        if (eval(config.get('constants', constant))[0] not in pd_df.keys()):
            #if its value is not empty (None) add it to new columns,
            #else a empty column (np.nan)
            if eval(config.get('constants', constant))[1] != None:
                new_columns[eval(config.get('constants', constant))[0]] = eval(config.get('constants', constant))[1]
            else: 
                new_columns[eval(config.get('constants', constant))[0]] = np.nan
    #add the new columns
    set_columns(pd_df, new_columns)
    
def add_additional_data(pd_df, config_path, selection = None, field_selection = None):
    """Add the additional data from the configuration file at config_path
    to the pandas DataFrame (pd_df)
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        Frame to change
    config_path : str
        path to the configuration file (including its name)
    selection : list of tuple or None
        Selection of additional data that should be used 
        and the column it is linked to.
        Form: [(column1, data1), (column2, data2), ...]
        (data1 is equal to the value in column1 and is the value the data is linked to)
        (optional, default: None)
    field_selection : list or None
        list of the names of fields from the additional data that should be used
        (optional, default: None)
    """
    config = configparser.ConfigParser()
    config.read(config_path)
    #no selection -> all listed additional data will be included
    if selection == None:
        selection = eval(config.get('additional Data', 'data'))
    #conditions to link the additional data to the fitting values
    conditions = [pd_df[data[0]] == data[1] for data in selection]
    all_fields = {}
    #for every additional data in selection
    for data in selection:
        #for every data field in the current data
        for field in config.options(data[1]):
            #if the field is not in all_fields
            #and it is in field_selection (or field_selection == None)
            #add it to all_fields
            if (field not in all_fields.keys()):
                if field_selection == None:
                    all_fields[field] = eval(config.get(data[1], field))[0]
                elif field in field_selection:
                    all_fields[field] = eval(config.get(data[1], field))[0]
    #Create a list with one list for each additional data.
    #In the sublists: for each field in all_fields,
    #if the field is in all_fields,
    #create a Series with the value of the field in it,
    #else create a np.nan Series 
    choices = [[(pd.Series(eval(config.get(data[1], field))[1], range(0, len(pd_df)))
                ) if (field in config.options(data[1])
                     ) else (pd.Series(np.nan, range(0, len(pd_df)))
                            ) for field in all_fields.keys()
               ] for data in selection]
    #choose the values fitting to the conditions or np.nan 
    linked_values = np.select(conditions, choices, np.nan)
    #add the new columns
    i = 0
    for key in all_fields.keys():
        pd_df[all_fields[key]] = linked_values[i]
        i = i + 1
            
def sort_columns(pd_df, config_path, new_order = [], delete_not_declared = True):
    """Sort the columns to a new order. 
    If the pandas Dataframe contains columns
    that are not in new_order (or configuration file if new_order == [])
    they could be deleted.
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        Frame to change
    config_path : str
        path to the configuration file (including its name)
    new_order : list
        list of the column names in the new order
        (optional, default: [])
    delete_not_declared : bool
        if True, columns that are not in new_order 
        (or configuration file if new_order == [])
        are deleted
        (optional, default: True)
    
    Returns
    -------
    pandas DataFrame
        Frame with new order
    """
    #construct new order from configuration file if it is empty
    if (len(new_order) == 0):
        config = configparser.ConfigParser()
        config.read(config_path)
    
        new_order = []
        #at first add all variable columns
        for column_pair in config.options('variables'):
            if eval(config.get('variables', column_pair))[0] in pd_df.keys():
                new_order.append(eval(config.get('variables', column_pair))[0])
        #then constant columns        
        for column_pair in config.options('constants'):
            if (eval(config.get('constants', column_pair))[0] not in new_order
               ) & (eval(config.get('constants', column_pair))[0] in pd_df.keys()):
                new_order.append(eval(config.get('constants', column_pair))[0])
        #at least at additional data field columns
        for data in eval(config.get('additional Data', 'data')):
            for column_pair in config.options(data[1]):
                if (eval(config.get(data[1], column_pair))[0] not in new_order
                   ) & (eval(config.get(data[1], column_pair))[0] in pd_df.keys()):
                    new_order.append(eval(config.get(data[1], column_pair))[0])
    #delete or add the missing columns depending on delete_not_declared
    for key in pd_df.keys():
        if key not in new_order:
            if delete_not_declared:
                pd_df.drop(key, axis = 1, inplace = True)
            else:
                new_order.append(key)
    
    return pd_df[new_order]

def set_rows(pd_df, rows, values):
    """set rows in a pandas DataFrame (pd_df) to new values
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        Frame to change
    rows : int, or list of int
        rows that should be set
    values : list
        list of lists.
        Each list contains the new values for one row.
        form: [[val1, val2, ...],[val1, val2, ...], ...]
    
    Returns
    -------
    pandas DataFrame
        Frame with new values
    """
    if not type(rows) == list:
        rows = [rows]
    value_index = 0
    for row in rows:
        pd_df.loc[row] = values[value_index]
        value_index = value_index + 1
        pd_df = pd_df.sort_index().reset_index(drop = True)
    return pd_df
    
def set_columns(pd_df, columns_values):
    """set columns in a pandas DataFrame (pd_df) to new values
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        Frame to change
    columns_values : dictionary
        keys are the columns to change and values are lists with new values.
        form: {column1: [value1, value2, ...], column2: value1, ...}
    
    Returns
    -------
    pandas DataFrame
        Frame with new values 
    """
    for element in columns_values.keys():
        pd_df[element] = columns_values[element]
    return pd_df
    
def set_values(pd_df, rows, columns_values):
    """set values of specific rows and columns
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        Frame to change
    columns_values : dictionary
        keys are the columns to change and values are lists with new values.
        form: {column1: [value1, value2, ...], column2: [value1, value2, ...], ...}
    rows : int, or list of int
        rows that should be set
    
    Returns
    -------
    pandas DataFrame
        Frame with new values
    """ 
    if not type(rows) == list:
        rows = [rows]
    for column in columns_values:
        for row in rows:
            pd_df.at[row, column] = columns_values[column]
            pd_df = pd_df.sort_index().reset_index(drop = True)
    return pd_df

def row_to_string(row_series, digits = 1000):
    """Constructs a string from all columns of a row.
    Only the first digits of a column are recognized.
    The row string contains ',' as a delimiter between columns.
    With that delimiter, rows with empty entries will not generate the same uuid.
    (except of intern hash collisions of course)
    
    Parameters
    ----------
    row_series : pandas Series 
        contains all values of a row
    digits : int
        number of charcters that are used from each value to generate the uuid
    
    Returns
    -------
    str 
        string with the ',' seperated values of the row.
    """
    #
    if len(row_series) > 0:
        row_string = str(row_series.iloc[0])[:digits]
        for i in range(1,len(row_series)):
            row_string = row_string + ',' + str(row_series.iloc[i])[:digits]
    else:
        row_string = ''
    return row_string

def generate_uuidv5_column(pandas_dataframe, namespace = None, column_name = 'ID', digits = 1000):
    """generates a uuid version 5 column from the columns of a pandas dataframe.
    The uuid from one row is constructed from the namespace 
    (if None: uuid of 'Laser-Plasma-Physik' with OID namespace) and a string of the row.
    This string is constructed by row_to_string function.
    It will only use the first digits of each column of one row.
    
    Parameters
    ----------
    pandas_dataframe : pandas DataFrame
        from this Frame the uuid column will be geenerated
    namespace : uuid object
        namespace for creating a uuid with uuidv5
        (optional, default: None)
    column_name : str
        name of the uuid column
        (optional, default: 'ID')
    digits : int
        number of charcters that are used from each value to generate the uuid
    """
    if namespace == None:
        namespace = uuid.uuid5(uuid.NAMESPACE_OID, 'Laser-Plasma-Physik')
    pandas_dataframe[column_name] = [uuid.uuid5(namespace, row_to_string(pandas_dataframe.iloc[row], digits)) for row in range(len(pandas_dataframe.index))]

def multi_change_files(pd_df, changes):
    """apply multiple changes on one file
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        Frame to change
    changes : list
        list of lists.
        Each list includes a str for the operation to do and the needed arguments.
        Take a look at the cases in the code to see what operations are available.
        The arguments depent on the used function.
        form:  [[operation1, operation spec. arguments],
                [operation2, operation spec. arguments],
                ...]
    
    Returns
    -------
    pandas DataFrame
        changed Frame
    """
    changed_frame = pd_df.copy()
    for change in changes:
        match change[0]:
            case 'delete_rows':
                changed_frame.drop(*change[1:], axis = 0, inplace = True)
            case 'delete_columns':
                changed_frame.drop(*change[1:], axis = 1, inplace = True)
            case 'set_rows':
                changed_frame = set_rows(changed_frame, *change[1:])
            case 'set_columns':
                changed_frame = set_columns(changed_frame, *change[1:])
            case 'set_values':
                changed_frame = set_values(changed_frame, *change[1:])
            case 'map_columns':
                map_columns(changed_frame, *change[1:])       
            case 'add_constants':
                add_constants(changed_frame, *change[1:])           
            case 'add_additional_data':
                add_additional_data(changed_frame, *change[1:])           
            case 'sort_columns':
                changed_frame = sort_columns(changed_frame, *change[1:])  
            case 'generate_uuidv5_column':
                generate_uuidv5_column(changed_frame, *change[1:])
            
    return changed_frame
