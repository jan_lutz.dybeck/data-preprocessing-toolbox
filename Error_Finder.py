
def check_frame_dtypes(pd_df, dtypes_of_columns):
    """check if the column dtypes of a pandas DataFrame (pd_df)
    fit to defined dtypes_of_columns
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        DataFrame to check
    dtypes_of_columns : dictionary
        specifiy the dtypes the columns should have
        form: {'column1': dtype1, 'column2': dtype2, ...} 
    
    Returns
    -------
    dictionary
        dictionary with found missfits.
        The keys are the columns and the values the dtypes
    """
    wrong_dtypes = {}
    for element in pd_df.dtypes.keys():
        if pd_df.dtypes[element] != dtypes_of_columns[element]:
            wrong_dtypes[element] = pd_df.dtypes[element]         
    return wrong_dtypes
    
def check_missing_columns(pd_df, dtypes_of_columns):
    """check if the columns in the frame are the same as in
    dtypes_of_columns
    
    Parameters
    ----------
    pd_df : pandas DataFrame
        DataFrame to check
    dtypes_of_columns : dictionary
        specifiy the dtypes the columns should have
        form: {'column1': dtype1, 'column2': dtype2, ...}
    
    Returns
    -------
    list
        list of tuple including column and were it is missing
    """
    missing_columns = []
    file_dtypes = pd_df.dtypes
    for input_column in dtypes_of_columns.keys():
        if input_column not in file_dtypes:
            missing_columns.append((input_column, 'DataFrame'))
    for file_column in file_dtypes.keys():
        if file_column not in dtypes_of_columns:
            missing_columns.append((file_column, 'dtypes_of_columns'))
    return missing_columns

def take_sample(pd_df, n_rows):
    """ Take a sample of the pandas DataFrame without 
    the need to call sample on frame directly

    Parameters
    ----------
    pd_df : pandas DataFrame
        DataFrame to sample
    n_rows : int
        amount of rows in the sample
    
    Returns
    -------
    pandas DataFrame
        sample of the frame
    """
    return pd_df.sample(n_rows, axis = 0)

def castable(value, cast_type, decimal = '.'):
    """ proofs if a value is castable to cast_type.
    Takes care of different decimal delimiters.
    This function is meant as a support function for scan_frame.

    Parameters
    ----------
    value : any pandas dtype
        value that should be proofed
    cast_type : any type with build in cast function of the form: cast_type(value)
        type that value should be casted on.
    decimal : str
        decimal delimiter in strings. Neccesary for example for casting '3,5' to float
        (optional, default: '.')

    Returns
    -------
    boolean
        True if it is castable, False if not
    """
    is_castable = True

    #if the value is a string some extra cases are proofed
    if (type(value) == str):
        #if the delimiter is ',' replace it with '.' and try to cast
        if (decimal == ',') & (',' in value):
            try:
                cast_type(value.replace('.','').replace(',','.'))
            except:
                is_castable = False
        #if not cast directly
        else:
            try:
                cast_type(value)
            except:
                is_castable = False
        #special case of extra values like np.nan
        #note: 'nan' is castable to float but i keep this if for
        #other cases that may occure.
        if is_castable == False:
            is_castable = True
            try:
                cast_type(eval(value))
            except:
                is_castable = False
    #for other types try to cast it
    else:
        try:
            cast_type(value)
        except:
            is_castable = False
    return is_castable

def scan_frame(frame, dtypes_of_columns, decimal = '.'):
    """ Scans the frame for errors by proofing if the values are castable to the wanted type.
    Iterates frame column wise.

    Parameters
    ----------
    frame : pandas DataFrame
        DataFrame to check
    dtypes_of_columns : dictionary
        specifiy the dtypes the columns should have
        form: {'column1': dtype1, 'column2': dtype2, ...}
    decimal : str
        decimal delimiter in strings. Neccesary for example for casting '3,5' to float
        (optional, default: '.')

    Returns
    -------
    dictionary
        dictionary with the errors in it. 
        Each column is saved in a Series including the line and type of the errors.
    """
    errors = {}
    for column in frame:
        #apply castable on a coulumn and save the sliced column with not castable values in errors
        errors[column] = frame[frame[column].apply(castable, cast_type = dtypes_of_columns[column], decimal = decimal) != True][column]
    return errors
    