from multiprocessing import Pool

def multiprocess_function(objects, function, function_arguments = None, processes = 5):
    """apply a function with given arguments to a dask DataFrame (ddf)

    Parameters
    ----------
    objects : list
        list of objects to process. (i.e. filepath)
    function : def
        function to multiprocess
    function_arguments : list
        list with lists with needed additional arguments for function.
        (optional, default = None)
    processes : int
        Number of simultaniously used processes
        (optional, default = 5)
            
    Returns
    -------
    list
        list of the returns of function for each file in files
    """
    if function_arguments == None:
        with Pool(processes=processes) as pool:
            result = pool.map(function, objects)
    else:
        #Set up a tuple list, that is needed for starmap.
        if len(function_arguments) == len(objects):
            argument_list = []
            i = 0
            for object in objects:
                argument_list.append((object, *function_arguments[i]))
                i = i + 1
        else:
            argument_list = [(object, *function_arguments[0]) for object in objects]
        with Pool(processes=processes) as pool:
            result = pool.starmap(function, argument_list)
    return result

def multiprocess_wrapper(file, function_and_arguments):
    """a wrapper that could wrap multiple functions with multiple arguments.
    functions will be applied in the order they are listed in function_and_arguments.
    The second function will be applied on the result of the first function.
    
    Parameters
    ----------
    file : str
        path to file to process
    function_and_arguments : list
        list with functions and there arguments.
        form: [[firstfunction, secondfunction, ...],[arguments for firstfunction, arguments for secondfunction, ...]]
        were arguments for functions are dictionaries and functions are expressions.
    """
    result = function_and_arguments[0][0](file, **function_and_arguments[1][0])
    if len(function_and_arguments[0]) > 1:
        i = 1
        for function in function_and_arguments[0][1:]:
            result = function(result, **function_and_arguments[1][i])
            i = i + 1
    return result